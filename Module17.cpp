
#include <iostream>
#include <math.h>

class Vector
{
private:
	double x;
	double y;
	double z;

public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	double GetSqrt() 
	{
		return sqrt((x * x) + (y * y) + (z * z));
	}


};



int main()
{
	Vector v(10,15,25);
	Vector v1(2, 4, 6);
	std::cout << "Vector modulus |a| = " << v.GetSqrt() << "\n" << "Vector modulus 2 |a| = " << v1.GetSqrt();

}

